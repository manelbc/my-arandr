#!/bin/python

import sys
import os
import subprocess
from datetime import datetime
import traceback
from time import sleep

def main():
    try:
        log('Starting execution')

        xrandr_reset()
        sleep(1) # For some reason xrandr only detects new video outputs after a second reset
        xrandr_reset()

        monitors = xrandr_get_available_monitors()
        xrandr_setup_monitors(monitors)
        i3_arrange_workspaces(monitors)
    except Exception:
        xrandr_reset()
        log('Something went wrong:')
        log(traceback.format_exc(), False)
    finally:
        log('', False)

def xrandr_reset():
    subprocess.run('xrandr --auto', shell=True, check=True)

def xrandr_get_available_monitors() -> list[str]:
    command = """xrandr --listmonitors | awk '/+/ { gsub("+|*",""); print $2 }'"""
    available_monitors = subprocess.run(command, capture_output=True, shell=True, check=True).stdout
    monitors = [m for m in available_monitors.split('\n') if m != '']
    monitors = filter_and_sort_monitors(monitors, ['eDP', 'DP'])
    if len(monitors) < 1:
        log('Failed to get available monitors')
        sys.exit(1)
    log(f'Monitors found: {monitors}')
    return monitors

def xrandr_get_video_outputs() -> list[str]:
    command = """xrandr | awk '/connected/ {print $1}'"""
    xrandr_out = subprocess.run(command, capture_output=True, shell=True, check=True).stdout
    return [m for m in xrandr_out.split('\n') if m != '']

def xrandr_disable_video_output(outputs: list[str]):
    log(f'Disabling: {outputs}')
    command = 'xrandr'
    for output in outputs:
        command += f' --output {output} --off'
    subprocess.run(command, shell=True, check=True)

def filter_and_sort_monitors(monitors: list[str], filters: list[str]) -> list[str]:
    total = []
    for flt in filters:
        total += sorted([m for m in monitors if m.startswith(flt)])
    return total

def xrandr_setup_monitors(monitors: list[str]):
    command = 'xrandr'
    resolution_x, resolution_y = 1920, 1080
    for i, monitor in enumerate(monitors):
        primary = '--primary' if i == 0 else ''
        output = f' --output {monitor} {primary} --mode {resolution_x}x{resolution_y} --pos {i * resolution_x}x0 --rotate normal'
        command += output

    log(f'Executing:\n\t {command}')
    try:
        subprocess.run(command, shell=True, check=True)
    except subprocess.CalledProcessError:
        log('Something went wrong when executing command')
        log('Defaulting to auto')
        xrandr_reset()
        sys.exit(1)

def i3_arrange_workspaces(monitors: list[str]):

    def i3_move_workspace_to(workspace: int, output: str):
        log(f'Moving workspace {workspace} to output {output}')
        command = f'i3-msg \'[workspace="{workspace}"]\' move workspace to output {output}'
        subprocess.run(command, shell=True, check=True)

    workspaces = list([1,2,3,4,5,6,7,8,9,0])
    for monitor in monitors:
        i3_move_workspace_to(workspaces.pop(0), monitor)

    for workspace in workspaces:
        i3_move_workspace_to(workspace, monitors[-1])

def log(msg: str, with_timestamp: bool = True):
    file_name = f'{os.path.dirname(os.path.abspath(__file__)).split("/")[-1]}.log'
    file_path = f'/tmp/{file_name}'

    if os.path.exists(file_path):
        access_mode = 'a'
    else:
        access_mode = 'w'

    with open(file_path, access_mode) as file:
        if with_timestamp:
            now = datetime.now().strftime('%d/%m/%Y-%H:%M:%S')
            line = f'{now} - {msg}\n'
        else:
            line = f'{msg}\n'
        file.write(line)

main()
